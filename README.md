# install webpack

```shell script
npm i -g webpack
npm i -g webpack-cli
npm link webpack-cli
```

## npm 

```shell script
npm init
```

```shell script
nmp i
```

1. devDependencies

```shell script
npm install module_nam --save-dev
npm i module_name -D
```

2. dependencies

```shell script
npm install module_name --save
npm i module_name -S
```

启动本地服务

```shell script
npm run server
```

## loader

1. 图片
```shell script
npm i --save-dev file-loader
npm i --save-dev url-loader
```

2. css

```shell script
npm i --save-dev style-loader
npm i --save-dev css-loader
```

分离css到单独的文件

```shell script
npm i --save-dev mini-css-extract-plugin
```