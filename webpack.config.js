const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TransferWebpackPlugin = require('transfer-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const glob = require('glob');
module.exports = {
    // entry: './src/home.js',
    entry: getEntry(),
    // devtool: 'source-map',
    devtool: 'eval-source-map',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name].js'
    },
    mode: 'development',
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),//本地服务器所加载的页面所在的目录
        historyApiFallback: true,//不跳转
        hot: true,//实时刷新
        port: 8080,
        openPage: 'pages/home.html'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            // you can specify a publicPath here
                            // by default it uses publicPath in webpackOptions.output
                            publicPath: '../',
                            hmr: process.env.NODE_ENV === 'development',
                        },
                    },
                    'css-loader']
            },
            {
                test: /\.scss$/,
                //css不分离写法
                // use:['style-loader','css-loader','sass-loader','postcss-loader'],
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                    }
                ]
            },
            {
                test: /\.(png|jpg|gif|ico)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 30000,
                            name: '[name].[hash:8].[ext]',
                            outputPath: './images',
                            publicPath: '../images'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
            chunkFileName: '[id].css'
        }),
        new webpack.ProvidePlugin({//全局引入jquery
            $: "jquery",
            jQuery: "jquery",
            jquery: "jquery",
            "window.jQuery": "jquery"
        }),
        new CleanWebpackPlugin(),
        // new TransferWebpackPlugin([
        //     {
        //         from: 'assets',
        //         to: 'assets'
        //     }
        // ], path.resolve(__dirname, "src")),
    ]
};

//读取src目录所有page入口
function getEntry() {
    const entry = {};
    glob.sync('./src/js/**/*.js').forEach(function (name) {
        const start = name.indexOf('src/') + 4;
        const end = name.length - 3;
        const eArr = [];
        let n = name.slice(start, end);
        n = n.split('/')[1];
        eArr.push(name);
        // eArr.push('babel-polyfill');
        entry[n] = eArr;
    });
    return entry;
}

//动态生成html
//获取html-webpack-plugin参数的方法
let getHtmlConfig = function (name) {
    return {
        template: `./src/pages/${name}.html`,
        filename: `pages/${name}.html`,
        inject: true,
        hash: false,
        chunks: [name]
    }
};
//配置页面
let entryObj = getEntry();
const htmlArray = [];
Object.keys(entryObj).forEach(function (element) {
    htmlArray.push({
        _html: element,
        title: '',
    })
});
//自动生成html模板
htmlArray.forEach(function (element) {
    module.exports.plugins.push(new HtmlWebpackPlugin(getHtmlConfig(element._html)));
});

