// function getByClassName(obj, cls) {
//     let elements = obj.getElementsByTagName("*");
//     const result = [];
//     for (let i = 0; i < elements.length; i++) {
//         if (elements[i].className === cls) {
//             result.push(elements[i])
//         }
//     }
//     return result;
// }
//
// function hasClass(obj, cls) {
//     return obj.className.match(new RegExp("(\\s|^)" + cls + "(\\s|$)"))
// }
//
// function removeClass(obj, cls) {
//     if (hasClass(obj, cls)) {
//         let reg = new RegExp("(\\s|^)" + cls + "(\\s|$)");
//         obj.className = obj.className.replace(reg, "");
//     }
// }
//
// function addClass(obj, cls) {
//     if (!hasClass(obj, cls)) {
//         obj.className += " " + cls;
//     }
// }
//
// window.onload = function () {
//     window.onscroll = function () {
//         let top = document.documentElement ? document.documentElement.scrollTop : document.body.scrollTop;
//         let items = document.getElementById("content").getElementsByClassName("item");
//         let currentId = "";
//         for (let i = 0; i < items.length; i++) {
//             let item = items[i];
//             let itemTop = item.offsetTop;
//             if (top > itemTop - 200) {
//                 currentId = item.id
//             } else {
//                 break
//             }
//         }
//         const menus = document.getElementById("menu").getElementsByTagName("a");
//         if (currentId) {
//             for (let i = 0; i < menus.length; i++) {
//                 let menu = menus[i];
//                 let hrefs = menu.href.split("#");
//                 if (hrefs[hrefs.length - 1] !== currentId) {
//                     removeClass(menu, "current")
//                 } else {
//                     addClass(menu, "current")
//                 }
//             }
//         }
//
//     }
// };
require('../css/mall.css');
$(document).ready(function () {
    $(window).scroll(function () {
        let top = $(document).scrollTop();
        let items = $("#content").find(".item");
        let currentId = "";
        items.each(function () {
            let m = $(this);
            let itemTop = m.offset().top;
            if (top > itemTop - 200) {
                currentId = "#" + m.attr("id");
            } else {
                return false;
            }
        });
        let menu = $("#menu");
        let currentLink = menu.find(".current");
        if (currentId && currentLink && currentLink.attr("href") !== currentId) {
            currentLink.removeClass("current");
            menu.find('a[href="' + currentId + '"]').addClass("current")
        }
    })
});